module gitee.com/mryy1996/pan123

go 1.18

require (
	github.com/PeterYangs/tools v0.2.52 // indirect
	github.com/axgle/mahonia v0.0.0-20180208002826-3358181d7394 // indirect
	github.com/go-resty/resty/v2 v2.10.0 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df // indirect
)
