package src

import (
	"crypto/md5"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/PeterYangs/tools"
	"github.com/go-resty/resty/v2"
	"io"
	"os"
	"strconv"
)

// BASEURL api域名
const BASEURL = "https://open-api.123pan.com"

type Pan123 struct {
	clientId     string
	clientSecret string
	client       *resty.Client
	token        string
}

type Info struct {
	Size         int64
	ParentFileID int
	Filename     string
	Md5          string
}

func NewPan123(clientId string, clientSecret string) *Pan123 {

	client := resty.New()

	client.SetHeaders(map[string]string{
		"Platform":     "open_platform",
		"Content-Type": "application/json",
		"User-Agent":   "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36",
	})

	client.SetBaseURL(BASEURL)

	client.SetRetryCount(1)

	return &Pan123{
		clientId:     clientId,
		clientSecret: clientSecret,
		client:       client,
	}
}

func (p *Pan123) GetAccessToken() (string, error) {

	rsp, err := p.client.R().SetBody(map[string]interface{}{
		"clientID":     p.clientId,
		"clientSecret": p.clientSecret,
	}).Post("/api/v1/access_token")

	if err != nil {

		return "", err
	}

	type Data struct {
		AccessToken string `json:"accessToken"`
		ExpiredAt   string `json:"expiredAt"`
	}

	type Res struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
		Data    Data   `json:"data"`
	}

	var res Res

	json.Unmarshal(rsp.Body(), &res)

	//fmt.Println(rsp)

	if res.Code != 0 {

		return "", errors.New(res.Message)

	}

	return res.Data.AccessToken, err

}

func (p *Pan123) Upload(parentFileID int, targetFilename string, srcPath string) (Info, error) {

	info, err := os.Stat(srcPath)

	if err != nil {

		return Info{}, err
	}

	md5Str, mErr := p.Md5File(srcPath)

	if mErr != nil {

		return Info{}, mErr
	}

	type CreateData struct {
		Reuse       bool   `json:"reuse"`
		PreuploadID string `json:"preuploadID"`
		SliceSize   int64  `json:"sliceSize"`
	}

	type CreateRsp struct {
		Code    int        `json:"code"`
		Message string     `json:"message"`
		Data    CreateData `json:"data"`
	}

	var r CreateRsp

	_, rErr := p.client.R().SetResult(&r).SetHeaders(map[string]string{
		"Authorization": p.token,
	}).SetBody(map[string]interface{}{
		"parentFileID": parentFileID,
		"filename":     targetFilename,
		"size":         info.Size(),
		"etag":         md5Str,
	}).Post("/upload/v1/file/create")

	if rErr != nil {

		return Info{}, rErr
	}

	if r.Code != 0 {

		return Info{}, errors.New(r.Message)
	}

	//秒传
	if r.Data.Reuse {

		return Info{Size: info.Size(), ParentFileID: parentFileID, Filename: targetFilename, Md5: md5Str}, nil
	}

	file, err := os.Open(srcPath)

	if err != nil {

		return Info{}, err
	}

	//缓冲字节
	buf := make([]byte, r.Data.SliceSize)

	sliceIndex := 1

	for {

		//读到缓冲，每次读取都会清空上次数据
		l, readErr := file.Read(buf)

		if readErr != nil {

			if readErr == io.EOF {

				break
			}

			return Info{}, readErr

		}

		type UploadUrlData struct {
			PresignedURL string `json:"presignedURL"`
			IsMultipart  bool   `json:"isMultipart"`
		}

		type UploadUrlRsp struct {
			Code    int           `json:"code"`
			Message string        `json:"message"`
			Data    UploadUrlData `json:"data"`
		}

		var u UploadUrlRsp

		_, uErr := p.client.R().SetResult(&u).SetHeaders(map[string]string{
			"Authorization": p.token,
		}).SetBody(map[string]interface{}{
			"preuploadID": r.Data.PreuploadID,
			"sliceNo":     sliceIndex,
		}).Post("/upload/v1/file/get_upload_url")

		if uErr != nil {

			return Info{}, uErr
		}

		if u.Code != 0 {

			return Info{}, errors.New(u.Message)

		}

		fmt.Println(u.Data.PresignedURL)

		uploadRsp, uploadErr := p.client.R().SetHeaders(map[string]string{}).SetBody(buf[:l]).Put(u.Data.PresignedURL)

		if uploadErr != nil {

			return Info{}, uploadErr
		}

		fmt.Println(uploadRsp.String())

		sliceIndex++

	}

	type PartData struct {
		PartNumber string `json:"partNumber"`
		Size       int64  `json:"size"`
		Etag       string `json:"etag"`
	}

	type PartList struct {
		Parts []PartData `json:"parts"`
	}

	type PartRsp struct {
		Code    int      `json:"code"`
		Message string   `json:"message"`
		Data    PartList `json:"data"`
	}

	var pr PartRsp

	gg, partErr := p.client.R().SetResult(&pr).SetHeaders(map[string]string{
		"Authorization": p.token,
	}).SetBody(map[string]interface{}{
		"preuploadID": r.Data.PreuploadID,
	}).Post("/upload/v1/file/list_upload_parts")

	fmt.Println(gg.String())

	if partErr != nil {

		return Info{}, partErr

	}

	if pr.Code != 0 {

		return Info{}, errors.New(pr.Message)
	}

	if len(pr.Data.Parts) != 0 {

		var size int64

		for _, datum := range pr.Data.Parts {

			size += datum.Size

		}

		if info.Size() != size {

			return Info{}, errors.New("文件验证失败，文件大小和本地不符")
		}

	}

	type CompleteData struct {
		Async     bool `json:"async"`
		Completed bool `json:"completed"`
	}

	type CompleteRsp struct {
		Code    int          `json:"code"`
		Message string       `json:"message"`
		Data    CompleteData `json:"data"`
	}

	var cr CompleteRsp

	_, completeErr := p.client.R().SetResult(&cr).SetHeaders(map[string]string{
		"Authorization": p.token,
	}).SetBody(map[string]interface{}{
		"preuploadID": r.Data.PreuploadID,
	}).Post("/upload/v1/file/upload_complete")

	if completeErr != nil {

		return Info{}, completeErr

	}

	if cr.Code != 0 {

		return Info{}, errors.New(cr.Message)
	}

	return Info{Size: info.Size(), ParentFileID: parentFileID, Filename: targetFilename, Md5: md5Str}, nil
}

func (p *Pan123) SetToken(token string) *Pan123 {

	p.token = token

	return p
}

func (p *Pan123) Share(shareName string, shareExpire int, fileIDList []string, sharePwd string) {

	params := map[string]interface{}{
		"shareName":   shareName,
		"shareExpire": shareExpire,
		"fileIDList":  tools.Join(",", fileIDList),
	}

	if sharePwd != "" {

		params["sharePwd"] = sharePwd
	}

	rsp, err := p.client.R().SetHeaders(map[string]string{
		"Authorization": p.token,
	}).SetBody(params).Post("/api/v1/share/create")

	if err != nil {

		fmt.Println(err)

		return
	}

	fmt.Println(rsp.String())

}

func (p *Pan123) FileList(parentFileId int, page int, limit int, orderBy string, orderDirection string, searchData string) {

	params := map[string]string{
		"parentFileId":   strconv.Itoa(parentFileId),
		"page":           strconv.Itoa(page),
		"limit":          strconv.Itoa(limit),
		"orderBy":        orderBy,
		"orderDirection": orderDirection,
	}

	if searchData != "" {

		params["searchData"] = searchData
	}

	rsp, err := p.client.R().SetHeaders(map[string]string{
		"Authorization": p.token,
	}).SetQueryParams(params).Get("/api/v1/file/list")

	if err != nil {

		fmt.Println(err)

		return
	}

	fmt.Println(rsp.String())

}

func (p *Pan123) Md5File(filename string) (string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer f.Close()
	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", h.Sum(nil)), nil
}
